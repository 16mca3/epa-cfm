<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'maintain-customers',
        ]);
        DB::table('permissions')->insert([
            'name' => 'maintain-project',
        ]);
        DB::table('permissions')->insert([
            'name' => 'maintain-categories',
        ]);
        DB::table('permissions')->insert([
            'name' => 'maintain-questions',
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-feedback',
        ]);
        DB::table('permissions')->insert([
            'name' => 'set-feedback',
        ]);
        
    }
}
