<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nikhil',
            'email' => 'nikhilnediyodath@gmail.com',
            'is_admin' => true,
            'permissions' => '["maintain-customers","maintain-project","maintain-categories","maintain-questions","create-feedback","set-feedback"]',
            'password' => bcrypt('12345678'),
        ]);
    }
}
