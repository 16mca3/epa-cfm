@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Questions</h1>
@stop

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all question --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Questions</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">SN</th>
                        <th>Text</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                    @foreach (App\Models\Question::all() as $question)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $question->text }}</td>
                        <td>{{ $question->category->name }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete Question" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $question->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <a href="{{ url('questions')}}/{{$question->id }}" class="btn btn-xs" title="Edit Question">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of question --}}

    {{-- Add new question --}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Question</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('questions') }}" method="post">
            @csrf
            <div class="row">
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Text</label>
                            <input type="text" name="text" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Question Name">
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select id="category" name="category" class="form-control select2">
                                @foreach (App\Models\QuestionCategory::all() as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    {{-- End of add new question --}}
</div>

@include('modals.delete', ['url' => url('questions')])
@stop
@push('js')
<script>
    $(document).ready(function () {
        $("#category").select2();
    });
</script>
@endpush