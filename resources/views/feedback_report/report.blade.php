@extends('adminlte::page')

@section('title', 'Report')

@section('content_header')
    <h1>Report</h1>
@stop

@section('content')

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all feedback_report --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Feedback Report</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">FN</th>
                        <th>Customer</th>
                        <th>Project Name</th>
                        <th>Date</th>
                        <th>Round</th>
                        <th>Report</th>
                    </tr>
                    @foreach (App\Models\QuestionCategory::all() as $question_category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $feedbackreport->name }}</td>
                        <td>{{ $feedbackreport->name }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete QuestionCategory" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $question_category->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <a href="{{ url('question_categories')}}/{{$question_category->id }}" class="btn btn-xs" title="Edit QuestionCategory">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of question_category --}}