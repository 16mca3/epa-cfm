<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notes</title>
    
    <link rel="stylesheet" href="{{ asset('std_lib/bootstrap/4.3.1/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/aqas.css') }}">
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> --}}
    
    @stack('css')
</head>

<body style="background-color: ghostwhite;">

    @include('ghostwhite.partials.nav_bar')
    @yield('content')

    <script src="{{ asset('std_lib/angularjs/1.6.9/angular.min.js') }}"></script>
    <script src="{{ asset('std_lib/jquery/3.4.0/jquery.min.js') }}"></script>
    <script src="{{ asset('std_lib/bootstrap/4.3.1/js/popper.min.js') }}"></script>
    <script src="{{ asset('std_lib/bootstrap/4.3.1/js/bootstrap.min.js') }}"></script>

    @stack('js')

</body>

</html>
