@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Projects</h1>
@stop

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all project --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Projects</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">SN</th>
                        <th>Name</th>
                        <th>Customers</th>
                        <th>Action</th>
                    </tr>
                    @foreach (App\Models\Project::all() as $project)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->customer_names }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete Project" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $project->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <a href="{{ url('projects')}}/{{$project->id }}" class="btn btn-xs" title="Edit Project">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of project --}}

    {{-- Add new project --}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Project</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('projects') }}" method="post">
            @csrf
            <div class="row">
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Project Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Customers</label>
                            <select id="customers" name="customers[]" class="form-control" multiple>
                                @foreach (App\Models\Customer::all() as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    {{-- End of add new project --}}
</div>

@include('modals.delete', ['url' => url('projects')])
@stop
@push('js')
<script>
    $(document).ready(function () {
        $("#customers").select2();
    });
</script>
@endpush