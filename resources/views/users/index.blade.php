@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Users</h1>
@stop

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all user --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Users</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">SN</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Permissions</th>
                        <th>Action</th>
                    </tr>
                    @foreach (App\Models\User::all() as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->permission_names }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete User" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $user->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            
                            <a href="{{ url('users')}}/{{$user->id }}" class="btn btn-xs" title="Edit User">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of user --}}

    {{-- Add new user --}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New User</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('users') }}" method="post">
            @csrf
            <div class="row">
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter User Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="text" name="password" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter User Email">
                        </div>
                        <div class="form-group">
                            <label>Permissions</label>
                            <select id="permissions" name="permissions[]" class="form-control" multiple>
                                @foreach (App\Models\Permission::all() as $permission)
                                <option>{{ $permission->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    {{-- End of add new user --}}
</div>

@include('modals.delete', ['url' => url('users')])
@stop
@push('js')
<script>
    $(document).ready(function () {
        $("#permissions").select2();
    });
</script>
@endpush