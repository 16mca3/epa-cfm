@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Customers</h1>
@stop

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all customer --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Customers</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">SN</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                    @foreach (App\Models\Customer::all() as $customer)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->phone }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete Customer" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $customer->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            
                            <a href="{{ url('customers')}}/{{$customer->id }}" class="btn btn-xs" title="Edit Customer">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of customer --}}

    {{-- Add new customer --}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Customer</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('customers') }}" method="post">
            @csrf
            <div class="row">
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Customer Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone</label>
                            <input type="text" name="phone" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Phone">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter Customer Email">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    {{-- End of add new customer --}}
</div>

@include('modals.delete', ['url' => url('customers')])
@stop