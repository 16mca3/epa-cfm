@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Feedback</h1>
@stop

@section('content')
<div class="col-md-10 col-xs-offset-1">
    {{-- Listing all feedback --}}
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Feedback</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">SN</th>
                        <th>Customer</th>
                        <th>Project</th>
                        <th>Round</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @foreach (App\Models\Feedback::all() as $feedback)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $feedback->customer->name }}</td>
                        <td>{{ $feedback->project->name }}</td>
                        <td>{{ $feedback->round->name }}</td>
                        <td>{{ $feedback->status }}</td>
                        <td>
                            <button type="button" class="btn btn-xs" title="Delete Feedback" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $feedback->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                            
                            <a href="{{ url('users')}}/{{$feedback->id }}" class="btn btn-xs" title="Edit Feedback">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    {{-- End of feedback --}}

    {{-- Add new feedback --}}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Create New Feedback</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('feedback') }}" method="post">
            @csrf
            <div class="row">
                <div class="box-body">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label>Customer</label>
                            <select id="customer" name="customer" class="form-control">
                                @foreach (App\Models\User::all() as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Project</label>
                            <select id="project" name="project" class="form-control">
                                @foreach (App\Models\Project::all() as $project)
                                <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Round</label>
                            <select id="round" name="round" class="form-control">
                                @foreach (App\Models\FeedbackRound::all() as $round)
                                <option value="{{ $round->id }}">{{ $round->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    {{-- End of add new feedback --}}
</div>

@include('modals.delete', ['url' => url('feedback')])
@stop
@push('js')
<script>
    $(document).ready(function () {
        $("#permissions").select2();
    });
</script>
@endpush