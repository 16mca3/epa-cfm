@extends('ghostwhite.app')
@section('content')
<div class="container my-5" ng-app="AQAS" ng-controller="AcademicAuditController">
    <div class="card p-5">
        <div class="card-header"><b>Feedback of @{{ title }}</b></div>
        <div ng-show="auditable_type=='program_audit'">
            <p><h6><b>Instructions</b></h6></p>
            <ul>
                <li>Documents to be verified will be show when you hover pointer on mark</li>
                <li>Check/Uncheck on the document will be applied automatically</li>
                <li>Click on the verified button to hide documents popup window</li>
                <li>Click on the keyaspect to view/edit documents again</li>
                <li>Remark/Observation is mandatory when mark is less than 4</li>
                <li>Click on <i class="fas fa-pencil-alt"></i> to add/edit remark</li>
                <li>Key aspect evaluation to be on a five point scale as follows
                    <ul>
                        <li>Not Relevant (NR)</li>
                        <li>Excellent (5)</li>
                        <li>Good (4)</li>
                        <li>Fair (3)</li>
                        <li>Poor (2)</li>
                        <li>Very Poor (1)</li>
                    </ul>
                </li>
            </ul>            
        </div>
        <div class="card-body p-0">
            <table class="table my-5 font-weight-normal" style="font-size: .9em;">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Question</th>
                        <th scope="col">Mark</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="row in table">
                        <td ng-if="row.category" colspan="3">@{{ row.title }}</td>
                        <th ng-if="!row.category" scope="row">@{{ $index + 1 }}</th>
                        <td ng-if="!row.category" ng-bind="row.question.text"></td>
                        <td ng-if="!row.category">
                            <label>
                                <input type="radio" class="ml-1" ng-model="row.question.mark" value="5">5
                            </label>
                            <label>
                                <input type="radio" class="ml-1" ng-model="row.question.mark" value="4">4
                            </label>
                            <label>
                                <input type="radio" class="ml-1" ng-model="row.question.mark" value="3">3
                            </label>
                            <label>
                                <input type="radio" class="ml-1" ng-model="row.question.mark" value="2">2
                            </label>
                            <label>
                                <input type="radio" class="ml-1" ng-model="row.question.mark" value="1">1
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button id="submit" type="button" ng-click="submit()" class="btn btn-primary float-right">Submit</button>
        </div>
    </div>
{{-- Error Modal --}}
    <!-- Modal -->
    <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="font-size:.8rem;">
                <p ng-repeat="error in errors">@{{ error }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
{{-- End of Error Modal --}}
</div>

@endsection

@push('js')
<script>
    var debug = true;
    var app = angular.module('AQAS', []);
    app.controller('AcademicAuditController', function ($scope, $http, $location) {
        var url = $location.protocol() + '://' + location.host + '/api/';
        $scope.feedback_id = '{{ $feedback_id }}';
        $scope.title = '{{ $title }}';
        $scope.table = [];

        // APIs
        $http.get(url + "get_questions/" + $scope.feedback_id)
            .then(function success(response) {
                $scope.categories = response.data;
                console.log('data', response.data);
                $scope.format();
            }, function error(params) {
                console.error('response', params);
            });

        $scope.submit = function (event) {
            $('#submit').prop("disabled", true);            
            if (!$scope.isValid()) { return; }
            $http({
                method : 'POST',
                url : url + "feedback_response",
                data : {
                    'feedback' : $scope.table,
                    'feedback_id' : $scope.feedback_id
                } })
            .then(function success(response) {
                    console.log('response', response);
                    if (response.data.message) { alert(response.data.message); }
                    if (response.data.redirect) { window.location.href = response.data.redirect; }
                }, function error(params) {
                    $('#submit').prop("disabled", false);    
                    console.error('response', params);
            });
        }

        $scope.format = function () {
            $scope.categories.forEach(category => {
                var data = {};
                data.title = category.name;
                data.category = true;
                $scope.table.push(data);
                category.questions.forEach(question => {
                    var data = {};
                    data.question = question;
                    $scope.table.push(data);
                });
            });
            console.log('table', $scope.table);
        }

        $scope.isValid = function () {
            if (debug) return true;
            if ($scope.auditable_type == 'department_audit') return true;
            $scope.errors = [];
            $scope.keyaspects.forEach(keyaspect => {
                if (keyaspect.mark == null) {
                    $scope.errors.push(keyaspect.id+'.'+keyaspect.title+' (required)');                    
                } else if (keyaspect.mark < 4 && keyaspect.remark == null) {
                    $scope.errors.push(keyaspect.id+'.'+keyaspect.title+' (remark required)');                    
                }
            });
            if ($scope.errors.length > 0) {
                $('#errorModal').modal('show');
                return false;
            }            
            return true;
        }
    });

</script>
@endpush

@push('css')
<style>
    .table td,
    .table th {
        padding-left: .4rem;
        padding-right: .4rem;
    }
</style>
@endpush
