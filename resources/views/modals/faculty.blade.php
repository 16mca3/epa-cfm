<div class="modal modal-default fade" id="facultyModal">
    <form action="{{ $url }}" method="post">
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Faculty</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter faculty name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="name" name="email" placeholder="Enter faculty eamil">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">KTU ID</label>
                    <input type="text" class="form-control" id="name" name="ktu_id" placeholder="Enter faculty ktu_id">
                </div>
                <div class="form-group">
                        <label for="exampleInputEmail1">Designation</label>
                        <select id="designation" name="designation" class="form-control" data-placeholder="Select department" style="width:100%">
                            @foreach (config('app.designations') as $designation)
                                <option>{{ $designation }}</option>
                            @endforeach
                        </select>
                    </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Department</label>
                    <select id="contacts" name="department" class="form-control" data-placeholder="Select department" style="width:100%">
                        @foreach (App\Models\Department::all() as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="action" class="btn btn-success">Save</button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div>