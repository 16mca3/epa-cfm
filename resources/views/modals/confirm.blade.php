<div class="modal fade" id="modalConfirm">
    <form action="{{ $url }}" method="POST">
        @csrf
        <input type="hidden" name="id" id="id">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure... &hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Yes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </form>
  </div>
  <!-- /.modal -->
  @push('js')
  <script>
      $("#{{ isset($modal_id) ? $modal_id : 'modalConfirm' }}").on('show.bs.modal', function (event) {
          $(this).find('#id').val($(event.relatedTarget).data('id'))
      });
  </script>
@endpush