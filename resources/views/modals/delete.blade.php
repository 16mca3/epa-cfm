<div class="modal modal-danger fade" id="{{ isset($modal_id) ? $modal_id : 'deleteModal' }}">
    <form action="{{ $url }}" method="post">
        @csrf
        @method('delete')
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline">Delete</button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div>

@push('js')
    <script>
        $("#{{ isset($modal_id) ? $modal_id : 'deleteModal' }}").on('show.bs.modal', function (event) {
            $(this).find('#id').val($(event.relatedTarget).data('id'))
        });
    </script>
@endpush