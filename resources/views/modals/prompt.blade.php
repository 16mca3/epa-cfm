<div class="modal modal-default fade" id="{{ isset($id) ? $id : 'promptModal' }}">
    <form action="{{ $url }}" method="post">
        <input type="hidden" name="_method" value="{{ isset($method) ? $method : 'POST' }}">
        @csrf
        <input type="hidden" name="id" id="id">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 id="title" class="modal-title">{{ isset($title) ? $title : 'Create new Item' }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label id="label">{{ isset($name) ? ucfirst($name) : 'Name' }}</label>
                    <input id="name" type="text" class="form-control" name="{{ isset($name) ? $name : 'name' }}" placeholder="{{ isset($placeholder) ? $placeholder : 'Enter Name' }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="action" class="btn btn-primary">Save</button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </form>
</div>
@push('js')
    <script>
        $("#{{ isset($id) ? $id : 'promptModal' }}").on('show.bs.modal', function (event) {
            $(this).find('#id').val($(event.relatedTarget).data('id'));
            $(this).find('#label').text($(event.relatedTarget).data('label'));
            $(this).find('#name').attr('name', $(event.relatedTarget).data('name'));
            $(this).find('#name').attr('placeholder', $(event.relatedTarget).data('placeholder'));
        });
    </script>
@endpush