<div class="modal fade" id="updateModal">
        <form action="{{ $url }}" method="post">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" id="id" value="">
            <input type="hidden" name="action" id="action" value="">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Update Department</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Department</label>
                    <select id="department" name="department_id" class="form-control" data-placeholder="Select department" style="width:100%">
                        @foreach (App\Models\Department::all() as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        </form>
      </div>
      <!-- /.modal -->
      @push('js')
      <script>
          $(document).ready(function() {
              $("#department").select2();
          } );
    
          $('#updateModal').on('show.bs.modal', function (event) {
              $(this).find('#id').val($(event.relatedTarget).data('id'));
              $(this).find('#action').val($(event.relatedTarget).data('action'));
          });
      </script>
    @endpush
    {{-- End of Select Auditor/Auditee Modal --}}
    