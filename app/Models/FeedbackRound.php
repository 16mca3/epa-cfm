<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackRound extends Model
{
    protected $fillable = [
        'name',
    ];

    // Accessors

    public function getQuestionCategoryNamesAttribute(){
        return $this->questionCategories ? implode(', ', $this->questionCategories->pluck('name')->toArray()) : NULL ;
    }

    // Relations

    public function questionCategories(){
        return $this->belongsToMany('App\Models\QuestionCategory');
    }
}
