<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'text',
    ];

    // Relations
    public function category(){
        return $this->belongsTo('App\Models\QuestionCategory', 'question_category_id');
    }
}
