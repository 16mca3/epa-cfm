<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
    ];
    // Accessors
    public function getCustomerNamesAttribute(){
        return $this->customers ? implode(', ', $this->customers->pluck('name')->toArray()) : NULL ;
    }

    // Relations
    public function customers(){
        return $this->belongsToMany('App\Models\Customer', 'project_customer');
    }
}
