<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    // Relations

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    public function project(){
        return $this->belongsTo('App\Models\Project', 'project_id');
    }
    public function round(){
        return $this->belongsTo('App\Models\FeedbackRound', 'round_id');
    }
}
