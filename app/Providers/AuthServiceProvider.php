<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-users', function ($user) {
            return $user->is_admin;
        });
        Gate::define('maintain-customers', function ($user) {
            return $user->permissions ? in_array('maintain-customers', $user->permissions) : NULL ;
        });
        Gate::define('maintain-project', function ($user) {
            return $user->permissions ? in_array('maintain-project', $user->permissions) : NULL ;
        });
        Gate::define('maintain-categories', function ($user) {
            return $user->permissions ? in_array('maintain-categories', $user->permissions) : NULL ;
        });
        Gate::define('maintain-questions', function ($user) {
            return $user->permissions ? in_array('maintain-questions', $user->permissions) : NULL ;
        });
        Gate::define('create-feedback', function ($user) {
            return $user->permissions ? in_array('create-feedback', $user->permissions) : NULL ;
        });
        Gate::define('set-feedback', function ($user) {
            return $user->permissions ? in_array('set-feedback', $user->permissions) : NULL ;
        });
    }
}
