<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('projects.index');
    }
    public function store(Request $request){
        // dump($request);return;   
        $project = Project::findOrNew($request->id);
        $project->fill($request->all());
        $project->save();
        $project->customers()->sync($request->customers);
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        // TODO validation and authorization
        $project = Project::find($request->id);
        $project->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
