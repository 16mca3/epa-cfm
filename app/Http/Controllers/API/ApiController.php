<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Models\Mark;
use App\Models\QuestionCategory;

class ApiController extends Controller
{
    public function getQuestions($feedback_id){
        return Feedback::find($feedback_id)->round->questionCategories()->with('questions')->get();
    }
    public function storeFeedback(Request $request){
        $feedback = Feedback::find($request->feedback_id);
        $marks = array();
        foreach ($request->feedback as $row) {
            if(isset($row['category'])){ continue; }
            $mark = new Mark([
                'feedback_id' => $feedback->id,
                'question_id' => $row['question']['id'],
                'mark' => isset($row['question']['mark']) ? $row['question']['mark'] : NULL ,
            ]);
            array_push($marks,$mark);
        }
        $feedback->marks()->saveMany($marks);
        $feedback->status = 'Completed';
        $feedback->save();
        return $marks;
    }
}
