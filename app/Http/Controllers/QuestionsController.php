<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('questions.index');
    }
    public function store(Request $request){
        $question = Question::findOrNew($request->id);
        $question->fill($request->all());
        $question->question_category_id = $request->category;
        $question->save();
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        $question = Question::find($request->id);
        $question->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
