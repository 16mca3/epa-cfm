<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('users.index');
    }
    public function store(Request $request){
        $user = User::findOrNew($request->id);
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->permissions = $request->permissions;
        $user->save();
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        // TODO validation and authorization
        $user = User::find($request->id);
        $user->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
