<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FeedbackRound;

class FeedbackRoundsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('rounds.index');
    }
    public function store(Request $request){
        $round = FeedbackRound::findOrNew($request->id);
        $round->fill($request->all());
        $round->save();
        $round->questionCategories()->sync($request->question_categories);
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        $round = FeedbackRound::find($request->id);
        $round->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
