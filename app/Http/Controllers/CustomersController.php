<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('customers.index');
    }
    public function store(Request $request){
        $customer = Customer::findOrNew($request->id);
        $customer->fill($request->all());
        $customer->save();
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        // TODO validation and authorization
        $customer = Customer::find($request->id);
        $customer->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
