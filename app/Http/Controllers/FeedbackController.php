<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('feedback.index');
    }
    public function store(Request $request){
        $feedback = Feedback::findOrNew($request->id);
        $feedback->customer_id = $request->customer;
        $feedback->project_id = $request->project;
        $feedback->round_id = $request->round;
        $feedback->save();
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        // TODO validation and authorization
        $feedback = Feedback::find($request->id);
        $feedback->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
