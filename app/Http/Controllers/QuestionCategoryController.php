<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionCategory;

class QuestionCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('question_categories.index');
    }
    public function store(Request $request){
        // dump($request);return;   
        $question_categories = QuestionCategory::findOrNew($request->id);
        $question_categories->fill($request->all());
        $question_categories->save();
        toastr()->success('Created Successfully');
        return back();
    }
    public function destroy(Request $request){
        // TODO validation and authorization
        $question_categories = QuestionCategory::find($request->id);
        $question_categories->delete();
        toastr()->success('Deleted Successfully');
        return back();
    }
}
