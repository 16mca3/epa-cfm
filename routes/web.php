<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'TestController@test');
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// UsersController
Route::get('users', 'UsersController@index');
Route::post('users', 'UsersController@store');
Route::delete('users', 'UsersController@destroy');

// CustomersController
Route::get('customers', 'CustomersController@index');
Route::post('customers', 'CustomersController@store');
Route::delete('customers', 'CustomersController@destroy');

// ProjectsController
Route::get('projects', 'ProjectsController@index');
Route::post('projects', 'ProjectsController@store');
Route::delete('projects', 'ProjectsController@destroy');

// QuestionCategoryController
Route::get('question_categories', 'QuestionCategoryController@index');
Route::post('question_categories', 'QuestionCategoryController@store');
Route::delete('question_categories', 'QuestionCategoryController@destroy');

// QuestionsController
Route::get('questions', 'QuestionsController@index');
Route::post('questions', 'QuestionsController@store');
Route::delete('questions', 'QuestionsController@destroy');

// FeedbackRoundsController
Route::get('rounds', 'FeedbackRoundsController@index');
Route::post('rounds', 'FeedbackRoundsController@store');
Route::delete('rounds', 'FeedbackRoundsController@destroy');

// FeedbackController
Route::get('feedback', 'FeedbackController@index');
Route::post('feedback', 'FeedbackController@store');
Route::delete('feedback', 'FeedbackController@destroy');